package main

import (
	"errors"
	"fmt"
)

func main() {

	numeros := []int{1, 2, 3}
	numerosDuplicados := aplicarOperacion(numeros, duplicar)
	fmt.Println(numerosDuplicados)

}

func sumar(a, b int) int { // int { ... }: Indica que la función devolverá un valor de tipo entero.
	resultado := a + b
	return resultado
}

// a, b int: Toma dos parámetros, a y b, ambos de tipo entero.
func dividir(a, b int) (int, error) { //(int, error) { ... }: Indica que la función devolverá dos valores: un entero y un valor de tipo error.
	if b == 0 {
		return 0, errors.New("no se puede dividir por cero")
	}
	resultado := a / b
	return resultado, nil
}

// el ... indica que la función puede recibir un número variable de parámetros es decir muchos parámetros/
func calcularPromedio(numeros ...int) float64 { //numeros ...int: Toma un número variable de parámetros de tipo entero. Estos se tratan como un slice dentro de la función.
	suma := 0
	for _, numero := range numeros { // range me devuelve el índice y el valor del slice. pero solo quiero el valor, por eso uso _.
		suma += numero
	}
	promedio := float64(suma) / float64(len(numeros))
	return promedio
}

func aplicarOperacion(numeros []int, operacion func(int) int) []int { //Toma una función como segundo parámetro. Esta función debe tomar un entero como entrada y devolver un entero como salida.
	resultado := make([]int, len(numeros))
	for i, numero := range numeros {
		resultado[i] = operacion(numero)
	}
	return resultado
}

func duplicar(numero int) int {
	return numero * 2
}
