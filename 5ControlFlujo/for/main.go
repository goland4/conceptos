package main

func main() {

	for i := 1; i <= 10; i++ {
		println(i)
		if i == 5 { // si i es igual a 5 se rompe el ciclo
			break
		}
	}

}
