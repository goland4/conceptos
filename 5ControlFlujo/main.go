package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {

	t := time.Now()
	hora := t.Hour()

	if hora < 12 {
		fmt.Println("Es de manana")
	} else if hora < 17 { //si la hora es menor que 17
		fmt.Println("es tarde")
	} else {
		fmt.Println(" es de noche")
	}

	os := runtime.GOOS //para ver el sistema operativo en que esta corriendo el programa
	switch os {
	case "windows":
		fmt.Println("go run -> Windows ")
	case "linux":
		fmt.Println("go run -> Linux ")
	case "darwin":
		fmt.Println("go run -> macOS")
	default:
		fmt.Println("go run -> otro OS")
	}
}
