package main

import "fmt"

func main() {

	// Declarar una matriz de 5 enteros
	var numeros [5]int
	fmt.Println(numeros) // Salida: [0 0 0 0 0]

	// Inicializar una matriz con valores
	var frutas = [3]string{"manzana", "pera", "plátano"}
	fmt.Println(frutas) // Salida: [manzana pera plátano]

	// Declaración e inicialización abreviada
	dias := [7]string{"lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo"}

	for i, dia := range dias {
		fmt.Println(i, dia)
	}

	///////////////////MATRICES MULTIDIMENSIONALES/////////////////////

	// Declarar una matriz de 3 filas y 2 columnas
	var tabla [3][2]int

	// Inicializar una matriz multidimensional
	var coordenadas = [2][3]int{{1, 2, 3}, {4, 5, 6}}

	fmt.Println(coordenadas[0][1]) // Salida: 2
	coordenadas[1][0] = 7          // Modificar un elemento

	for i, fila := range tabla {
		for j, valor := range fila {
			fmt.Printf("tabla[%d][%d] = %d\n", i, j, valor)
		}
	}
}
