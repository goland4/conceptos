package main

import "fmt"

func main() {
	// Declarar un slice vacío de enteros
	var numeros []int
	fmt.Println(numeros) // Salida: []

	// Inicializar un slice con valores
	var frutas = []string{"manzana", "pera", "plátano"}

	fmt.Println(len(frutas))               //longitud del slice
	fmt.Println(cap(frutas))               //capacidad del slice
	fmt.Println(append(frutas, "naranja")) //agrego un elemento al slice frutas, el append me devuelve un nuevo slice

	// Crear un slice usando make()
	dias := make([]string, 5) // Crea un slice con 5 elementos (inicializados a cero), el 5 es la longitud o capacidad del slice
	dias[0] = "lunes"

	// Crear un slice a partir de una matriz
	var matriz = [5]int{1, 2, 3, 4, 5}
	slice := matriz[1:3] // slice contiene {2, 3}
}
